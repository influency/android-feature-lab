package com.influency.itlab.http.base;


/**
 * Created by John_tp on 2018/2/26 0026.
 */

public class ApiMap {

    protected static final String DOMAIN_OFFICIAL = "https://fy-gateway.fengyuncx.com:8900";
    //    protected static final String DOMAIN_TEST = "tt.fy96799.com";
//    protected static final String DOMAIN_TEST_SPRING = "https://he1106.natapp4.cc:8900";
    protected static final String DOMAIN_TEST_SPRING = "https://test-gateway.fengyuncx.com";

    public static final String DOMAIN_TEST_PC = "https://he1106.natapp4.cc";

    /**
     * 用户协议地址
     */
    public static String AGREEMENT_URL = "http://www.fengyuncx.com/xieyi.html";
    /**
     * 图片地址前缀
     */
    public static String IMG_URL_PATH = "https://resource.fengyuncx.com";

    protected static final String DOMAIN = DOMAIN_TEST_SPRING;
    protected static final String BASE_URL = DOMAIN+"/";
    protected static final String SERVICE_NODE_BUSINESS = "business/";
    protected static final String SERVICE_NODE_SET = "fy/set-service/";
    protected static final String SERVICE_NODE_COMMON = "fy/common-service/";
    protected static final String VERSION = "v1/";

    /**
     * business-node
     */
    protected static final String BASE_SERVICE_URL_BUSINESS = BASE_URL + SERVICE_NODE_BUSINESS + VERSION;
    /**
     * order-node
     */

    /**
     * set-node配置信息
     */
    protected static final String BASE_SERVICE_URL_SET = BASE_URL + SERVICE_NODE_SET + "v6/";

    /**
     * common-node配置信息
     */
    protected static final String BASE_SERVICE_URL_COMMON = BASE_URL + SERVICE_NODE_COMMON + "v6/";



    /**
     * -------基础类别------------------------start------------------------------------------
     */

    //getinfo
    protected static final String GET_INFO = "passenger/v1/app/getInfo";
    //order list
//    protected static final String GET_ORDER_LIST = "passenger/v1/orderList/list";
    //coupon list
    protected static final String LIST_COUPONS = "passenger/v1/coupon/getCoupons";

    //检测版本
    protected static final String CHECK_VERSION = BASE_SERVICE_URL_SET + "sys/config/validateVersionForAndroid";
    //发送验证码
    protected static final String READY_CODE = BASE_SERVICE_URL_BUSINESS + "base/readySendCheck";
    //发送验证码
    protected static final String CHECK_CODE = BASE_SERVICE_URL_BUSINESS + "base/sendCheck";
    //获取外部链接
    protected static final String CITY_CONFIG_BY_POINT = BASE_SERVICE_URL_BUSINESS + "base/getModuleByLatLng";//*
    protected static final String CITY_CONFIG_BY_CITY = BASE_SERVICE_URL_BUSINESS + "base/getModuleByCity";
    protected static final String COMMON_URL = BASE_URL + "/fy-passenger/common/" + VERSION;//*
    //字典接口
    protected static final String GET_DICT_LIST = BASE_SERVICE_URL_COMMON + "dict/getDictByBean";//*
    //获取所有省份
    protected static final String GET_PROVINCES = BASE_SERVICE_URL_COMMON + "area/getProvinces";//*
    //获取所有城市
    protected static final String GET_ALL_CITIES = BASE_SERVICE_URL_SET + "area/getCitys";
    //获取相关图片
    protected static final String GET_PIC = BASE_SERVICE_URL_SET + "sys/pic/getPic";
    //获取阿里oss上传参数(非隐私图片)
    public static final String GET_ALI_OSS_POLICY_PUBLIC = BASE_SERVICE_URL_COMMON +"oss/getFyOssPolicy";
    //获取阿里oss隐藏文件上传参数
    protected static final String GET_ALI_OSS_POLICY_PRIVATE = BASE_SERVICE_URL_COMMON + "oss/getPolicy";
    //获取加密文件路径
    public static final String GET_ALI_PIC = BASE_SERVICE_URL_COMMON + "oss/getOssUrl";
    //根据行政编码获取城市列表
    protected static final String GET_CITIES = GET_PROVINCES;//*


    /** -------基础类别------------------------end------------------------------------------*/

    /**
     * -------用户相关------------------------start-------------------------------------------
     */


    // 注册或登录
    protected static final String CHECK_CODE_LOGIN = BASE_SERVICE_URL_BUSINESS + "base/checkCodeLogin";//
    protected static final String SIGN_UP = BASE_SERVICE_URL_BUSINESS + "base/register";//
    protected static final String FIND_PASSWORD = BASE_SERVICE_URL_BUSINESS + "base/checkCodePassword";//
    protected static final String PASSWORD_LOGIN = BASE_SERVICE_URL_BUSINESS + "company/mobileLogin";
    protected static final String LOGOUT = BASE_SERVICE_URL_BUSINESS + "company/mobileLogout";
    //启动获取信息
    protected static final String GET_START_INFO = BASE_SERVICE_URL_BUSINESS + "base/getInfo";
    //获取用户信息
    protected static final String GET_USER_INFO = BASE_SERVICE_URL_BUSINESS + "passengerInfo/getPassenger";
    //获取司机信息
    protected static final String GET_DRIVER_INFO = BASE_SERVICE_URL_BUSINESS + "passengerInfo/getDriverById";
    //退出
    protected static final String UPDATE_PASSWORD = BASE_SERVICE_URL_BUSINESS + "company/user/updatePassword";
    // 修改个人资料
    protected static final String USER_UPDATE = BASE_SERVICE_URL_BUSINESS + "passengerInfo/updatePassenger";//*
    // 修改头像
    protected static final String UPDATE_USER_PHOTO = BASE_SERVICE_URL_BUSINESS + "company/passenger/updateUser";
    // 获取常用地址
    protected static final String USUAL_ADDRESS = BASE_SERVICE_URL_BUSINESS + "passengerInfo/getAddress";
    // 保存常用地址
    protected static final String SAVE_USUAL_ADDRESS = BASE_SERVICE_URL_BUSINESS + "passengerInfo/saveAddress";
    // 删除常用地址
    protected static final String DELETE_USUAL_ADDRESS = BASE_SERVICE_URL_BUSINESS + "passengerInfo/deleteAddress";


    /** -------用户相关------------------------end------------------------------------------- */




    /** -------订单服务------------------------start------------------------------------------- */
    //城内订单详情
    protected static final String GET_INCITY_ORDER_DETAILS = BASE_SERVICE_URL_BUSINESS + "order/inside/getOrderById";
    //尊享包车订单详情
    protected static final String GET_RENT_ORDER_DETAILS = BASE_SERVICE_URL_BUSINESS + "order/rent/getOrderById";
    //行程列表
    protected static final String GET_ORDER_LIST = BASE_SERVICE_URL_BUSINESS + "order/getOrdersByPassenger";
    //待审批列表
    protected static final String GET_ORDER_AUDITING = BASE_SERVICE_URL_BUSINESS + "order/auditing/toAudited";
    //已审批列表
    protected static final String GET_ORDER_AUDITED = BASE_SERVICE_URL_BUSINESS + "order/auditing/audited";
    //租车计价信息
    protected static final String GET_RENT_PRICE_INFO = BASE_SERVICE_URL_BUSINESS + "order/rent/getRent";
    //城内计价信息
    protected static final String GET_INSIDE_ON_POINT = BASE_SERVICE_URL_BUSINESS + "order/inside/getOnPoints";
    //城内添加订单
    protected static final String POST_ADD_INSIDE_ORDER = BASE_SERVICE_URL_BUSINESS + "order/inside/addOrderForPassenger";
    //尊享包车添加订单
    protected static final String POST_ADD_BUSINESS_ORDER = BASE_SERVICE_URL_BUSINESS + "order/rent/addOrderForPassenger";

    //审核订单
    protected static final String POST_AUDITING_ORDER = BASE_SERVICE_URL_BUSINESS + "order/auditing/auditing";
    //城内确认到达
    protected static final String POST_INSIDE_CONFIRM_ARRIVE = BASE_SERVICE_URL_BUSINESS + "order/inside/confirm";
    //租车确认到达
    protected static final String POST_RENT_CONFIRM_ARRIVE = BASE_SERVICE_URL_BUSINESS + "order/rent/confirm";
    //城内订单评价
    protected static final String POST_INSIDE_RATE = BASE_SERVICE_URL_BUSINESS + "order/inside/rated";
    //租车订单评价
    protected static final String POST_RENT_RATE = BASE_SERVICE_URL_BUSINESS + "order/rent/rated";
    //催单
    protected static final String GET_AUDITING_NOTICE = BASE_SERVICE_URL_BUSINESS + "order/auditing/nextAuditingNotice";


    //取消租车订单
    protected static final String ORDER_CANCEL_RENT = BASE_SERVICE_URL_BUSINESS + "order/rent/cancelForPassenger";
    //取消城内订单
    protected static final String ORDER_CANCEL_INSIDE = BASE_SERVICE_URL_BUSINESS + "order/inside/cancelForPassenger";

    /** -------订单服务------------------------end------------------------------------------- */



    /**
     * ----------------------------百度api----------------------------------
     */
    protected static final String BAIDU_GET_ROUTS = "http://api.map.baidu.com/routematrix/v2/driving";
    protected static final String BAIDU_GET_WALK_ROUTS = "http://api.map.baidu.com/routematrix/v2/driving";
    protected static final String BAIDU_SEARCH_ADDRESS = "http://api.map.baidu.com/place/v2/search";
    /**
     * ------百度鹰眼api----
     */
    //关键词搜索
    protected static final String BAIDU_LOCATION_SEARCH = "http://yingyan.baidu.com/api/v3/entity/search";
    //圆形范围搜索
    protected static final String BAIDU_AROUND_SEARCH = "http://yingyan.baidu.com/api/v3/entity/aroundsearch";
    //矩形范围搜索
    protected static final String BAIDU_BOUND_SEARCH = "http://yingyan.baidu.com/api/v3/entity/boundsearch";
    //鹰眼轨迹搜索
    protected static final String BAIDU_TRACK_SEARCH = "http://yingyan.baidu.com/api/v3/track/gettrack";

    public static final int REQUEST_GET_VERSION = 1001;
    public static final int REQUEST_SEND_CODE = 1002;
    public static final int REQUEST_LOGIN = 1003;
    public static final int REQUEST_CHECK_TOKEN = 1004;
    public static final int REQUEST_EXIT = 1005;
    public static final int REQUEST_ORDER_TYPE = 1006;
    public static final int REQUEST_GET_LINE = 1007;
    public static final int REQUEST_GET_WEBS = 1008;
    public static final int REQUEST_GET_PROVICES = 1009;
    public static final int REQUEST_GET_CITIES = 1010;
    public static final int REQUEST_GET_ALL_CITIES = 1011;
    public static final int REQUEST_SIGN_UP = 10012;
    public static final int REQUEST_FIND_PASS = 10013;
    public static final int REQUEST_UPDATE_PASS = 10014;


}

package com.influency.itlab.http.model;

import java.util.List;

public class ActivateInfoModel {
    String picRootUrl;
    String cityCode;
    String passengerAgreement;
    String env;
    List<ServiceModule> modules;
    List<Market> market;
    String prepaymentAgreement;
    String customerServiceTelephone;
    PrivacyMap privacyMap;
    String payAgreement;
    String cityName;
    int outsidePlanDays;
    int insidePlanDays;
    String invoiceUrl;

    public String getPicRootUrl() {
        return picRootUrl;
    }

    public void setPicRootUrl(String picRootUrl) {
        this.picRootUrl = picRootUrl;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getPassengerAgreement() {
        return passengerAgreement;
    }

    public void setPassengerAgreement(String passengerAgreement) {
        this.passengerAgreement = passengerAgreement;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public List<ServiceModule> getModules() {
        return modules;
    }

    public void setModules(List<ServiceModule> modules) {
        this.modules = modules;
    }

    public List<Market> getMarket() {
        return market;
    }

    public void setMarket(List<Market> market) {
        this.market = market;
    }

    public String getPrepaymentAgreement() {
        return prepaymentAgreement;
    }

    public void setPrepaymentAgreement(String prepaymentAgreement) {
        this.prepaymentAgreement = prepaymentAgreement;
    }

    public String getCustomerServiceTelephone() {
        return customerServiceTelephone;
    }

    public void setCustomerServiceTelephone(String customerServiceTelephone) {
        this.customerServiceTelephone = customerServiceTelephone;
    }

    public PrivacyMap getPrivacyMap() {
        return privacyMap;
    }

    public void setPrivacyMap(PrivacyMap privacyMap) {
        this.privacyMap = privacyMap;
    }

    public String getPayAgreement() {
        return payAgreement;
    }

    public void setPayAgreement(String payAgreement) {
        this.payAgreement = payAgreement;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getOutsidePlanDays() {
        return outsidePlanDays;
    }

    public void setOutsidePlanDays(int outsidePlanDays) {
        this.outsidePlanDays = outsidePlanDays;
    }

    public int getInsidePlanDays() {
        return insidePlanDays;
    }

    public void setInsidePlanDays(int insidePlanDays) {
        this.insidePlanDays = insidePlanDays;
    }

    public String getInvoiceUrl() {
        return invoiceUrl;
    }

    public void setInvoiceUrl(String invoiceUrl) {
        this.invoiceUrl = invoiceUrl;
    }


    @Override
    public String toString() {
        return "ActivateInfoModel{" +
                "picRootUrl='" + picRootUrl + '\'' +
                ", cityCode='" + cityCode + '\'' +
                ", passengerAgreement='" + passengerAgreement + '\'' +
                ", env='" + env + '\'' +
                ", modules=" + modules +
                ", market=" + market +
                ", prepaymentAgreement='" + prepaymentAgreement + '\'' +
                ", customerServiceTelephone='" + customerServiceTelephone + '\'' +
                ", privacyMap=" + privacyMap +
                ", payAgreement='" + payAgreement + '\'' +
                ", cityName='" + cityName + '\'' +
                ", outsidePlanDays=" + outsidePlanDays +
                ", insidePlanDays=" + insidePlanDays +
                ", invoiceUrl='" + invoiceUrl + '\'' +
                '}';
    }

    public static class PrivacyMap {
        String newTitle;
        String newContent;
        String personalUrl;
        String title;
        String name2;
        String version;
        String name1;
        String url;
        String content;

        public PrivacyMap() {
        }

        public String getNewTitle() {
            return newTitle;
        }

        public void setNewTitle(String newTitle) {
            this.newTitle = newTitle;
        }

        public String getNewContent() {
            return newContent;
        }

        public void setNewContent(String newContent) {
            this.newContent = newContent;
        }

        public String getPersonalUrl() {
            return personalUrl;
        }

        public void setPersonalUrl(String personalUrl) {
            this.personalUrl = personalUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getName2() {
            return name2;
        }

        public void setName2(String name2) {
            this.name2 = name2;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getName1() {
            return name1;
        }

        public void setName1(String name1) {
            this.name1 = name1;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {

            this.content = content;
        }

        @Override
        public String toString() {
            return "PrivacyMap{" +
                    "newTitle='" + newTitle + '\'' +
                    ", newContent='" + newContent + '\'' +
                    ", personalUrl='" + personalUrl + '\'' +
                    ", title='" + title + '\'' +
                    ", name2='" + name2 + '\'' +
                    ", version='" + version + '\'' +
                    ", name1='" + name1 + '\'' +
                    ", url='" + url + '\'' +
                    ", content='" + content + '\'' +
                    '}';
        }
    }

    public static class Market {
        int id;
        String cityCode;
        String pic;
        String url;
        int seq;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCityCode() {
            return cityCode;
        }

        public void setCityCode(String cityCode) {
            this.cityCode = cityCode;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {

            this.seq = seq;
        }

        @Override
        public String toString() {
            return "Market{" +
                    "id=" + id +
                    ", cityCode='" + cityCode + '\'' +
                    ", pic='" + pic + '\'' +
                    ", url='" + url + '\'' +
                    ", seq=" + seq +
                    '}';
        }
    }

    public static class ServiceModule {
        int id;
        String cityCode;
        String cityName;
        int moduleId;
        String moduleName;
        int seq;
        String content;
        String bottomContent;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCityCode() {
            return cityCode;
        }

        public void setCityCode(String cityCode) {
            this.cityCode = cityCode;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public int getModuleId() {
            return moduleId;
        }

        public void setModuleId(int moduleId) {
            this.moduleId = moduleId;
        }

        public String getModuleName() {
            return moduleName;
        }

        public void setModuleName(String moduleName) {
            this.moduleName = moduleName;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getBottomContent() {
            return bottomContent;
        }

        public void setBottomContent(String bottomContent) {
            this.bottomContent = bottomContent;
        }

        @Override
        public String toString() {
            return "ServiceModule{" +
                    "id=" + id +
                    ", cityCode='" + cityCode + '\'' +
                    ", cityName='" + cityName + '\'' +
                    ", moduleId=" + moduleId +
                    ", moduleName='" + moduleName + '\'' +
                    ", seq=" + seq +
                    ", content='" + content + '\'' +
                    ", bottomContent='" + bottomContent + '\'' +
                    '}';
        }
    }

}
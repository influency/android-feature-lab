package com.influency.itlab.http.base;
import com.influency.itlab.http.model.ActivateInfoModel;
import com.influency.itlab.http.model.Coupon;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface Api {
//    static LatLng defaultLatLng = LatLng(39.909187,116.397451);

    @GET(ApiMap.GET_INFO)
    Single<ActivateInfoModel> getActivateInfo(@Query("lat") String lat, @Query("lng") String lng,@Query("terminal") int terminal);

    @GET(ApiMap.LIST_COUPONS)
    Single<List<Coupon>> listCoupons();
}

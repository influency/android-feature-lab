package com.influency.itlab.http.convert;

import java.io.IOException;

public class BaseResultException extends IOException {
    private String msg;
    private int code;

    public BaseResultException(String msg, int code) {
        this.msg = msg;
        this.code = code;
    }

    public BaseResultException(String message, String msg, int code) {
        super(message);
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
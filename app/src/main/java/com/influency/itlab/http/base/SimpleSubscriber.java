package com.influency.itlab.http.base;


import android.util.Log;

import com.influency.itlab.http.convert.BaseResultException;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public abstract  class SimpleSubscriber<T> implements Subscriber<T> {
    @Override
    public void onSubscribe(Subscription s) {
        s.request(Integer.MAX_VALUE);
    }

    @Override
    public void onError(Throwable t) {
        if (t instanceof BaseResultException) {
            BaseResultException resultException = (BaseResultException) t;
            Log.d(SimpleSubscriber.class.getSimpleName(), "Code: " + resultException.getCode() + "Message:" + resultException.getMessage());
        }
    }

    @Override
    public void onComplete() {

    }
}

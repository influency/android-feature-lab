package com.influency.itlab.http.base;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class ParameterizedTypeImpl implements ParameterizedType {
    private final Class rawType;
    private final Type[] actualTypes;

    public ParameterizedTypeImpl(Class rawType, Type[] actualTypes) {
        this.rawType = rawType;
        this.actualTypes = actualTypes != null?actualTypes:new Type[0];
    }

    @Override
    public Type[] getActualTypeArguments() {
        return actualTypes;
    }

    @Override
    public Type getRawType() {
        return rawType;
    }

    @Override
    public Type getOwnerType() {
        return null;
    }
}

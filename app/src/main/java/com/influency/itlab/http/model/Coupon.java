package com.influency.itlab.http.model;

import java.io.Serializable;

public class Coupon implements Serializable {
//    {
//            "id": 5000,
//            "orgId": 28,
//            "orderType": 2031,
//            "lineId": 2,
//            "carType": 1,
//            "mode": -1,
//            "couponMoney": 500,
//            "state": 0,
//            "couponType": 1,
//            "dateType": 2,
//            "couponDays": 10,
//            "branchProportion": 0.0,
//            "platformProportion": 100.0,
//            "name": "宜昌接送机5元优惠券",
//            "createDate": 1605800002000,
//            "updateDate": 1605800002000
//        }
    private long id;
    private long orgId;
    private int orderType;
    private long lineId;
    private int carType;
    private int mode;
    private long couponMoney;
    private int state;
    private int couponType;
    private int dateType;
    private int couponDays;
    private double branchProportion;
    private double platformProportion;
    private long createDate;
    private long updateDate;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOrgId() {
        return orgId;
    }

    public void setOrgId(long orgId) {
        this.orgId = orgId;
    }

    public int getOrderType() {
        return orderType;
    }

    public void setOrderType(int orderType) {
        this.orderType = orderType;
    }

    public long getLineId() {
        return lineId;
    }

    public void setLineId(long lineId) {
        this.lineId = lineId;
    }

    public int getCarType() {
        return carType;
    }

    public void setCarType(int carType) {
        this.carType = carType;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public long getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(long couponMoney) {
        this.couponMoney = couponMoney;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getCouponType() {
        return couponType;
    }

    public void setCouponType(int couponType) {
        this.couponType = couponType;
    }

    public int getDateType() {
        return dateType;
    }

    public void setDateType(int dateType) {
        this.dateType = dateType;
    }

    public int getCouponDays() {
        return couponDays;
    }

    public void setCouponDays(int couponDays) {
        this.couponDays = couponDays;
    }

    public double getBranchProportion() {
        return branchProportion;
    }

    public void setBranchProportion(double branchProportion) {
        this.branchProportion = branchProportion;
    }

    public double getPlatformProportion() {
        return platformProportion;
    }

    public void setPlatformProportion(double platformProportion) {
        this.platformProportion = platformProportion;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Coupon{" +
                "id=" + id +
                ", orgId=" + orgId +
                ", orderType=" + orderType +
                ", lineId=" + lineId +
                ", carType=" + carType +
                ", mode=" + mode +
                ", couponMoney=" + couponMoney +
                ", state=" + state +
                ", couponType=" + couponType +
                ", dateType=" + dateType +
                ", couponDays=" + couponDays +
                ", branchProportion=" + branchProportion +
                ", platformProportion=" + platformProportion +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                ", name='" + name + '\'' +
                '}';
    }
}

package com.influency.itlab.http.base;

import android.util.Log;

import com.influency.itlab.http.model.ActivateInfoModel;
import com.influency.itlab.http.model.Coupon;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class NetEngine {
    static final String TAG = NetEngine.class.getSimpleName();
    private static Api apiService = RetrofitManager.getInstance().create(Api.class);

    public static Disposable getInfo(String lat,String lng,Consumer<ActivateInfoModel> observer){
        return setSubscribe(apiService.getActivateInfo(lat,lng,21),observer);
    }
    public static Disposable listCoupons(Consumer<List<Coupon>> observer){
        return setSubscribe(apiService.listCoupons(),observer);
    }

    private static <T> Disposable setSubscribe(Single<T> observable, Consumer<T> observer){
        return setSubscribe(observable, observer, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable){
                Log.e(TAG,throwable.toString());
            }
        });
    }
    private static <T> Disposable setSubscribe(Single<T> observable, Consumer<T> observer, Consumer<Throwable> onError){
        return observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer,onError);
    }
}

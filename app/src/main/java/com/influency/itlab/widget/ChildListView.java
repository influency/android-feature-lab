package com.influency.itlab.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListView;

public class ChildListView extends ListView {
    public ChildListView(Context context) {
        super(context);
    }

    public ChildListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ChildListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    float clickX,clickY;
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()){
            case MotionEvent.ACTION_DOWN:

                super.requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_MOVE:
                float offsetX,offsetY;
                offsetX = Math.abs(ev.getX() - clickX);
                offsetY = Math.abs(ev.getY() - clickY);
                if(offsetX>offsetY){
                    super.requestDisallowInterceptTouchEvent(false);
                }else {
                    super.requestDisallowInterceptTouchEvent(true);
                }
                break;
            case MotionEvent.ACTION_UP:

                break;
            default:
                break;
        }
        clickX = ev.getX();
        clickY = ev.getY();
        return super.dispatchTouchEvent(ev);
    }
}

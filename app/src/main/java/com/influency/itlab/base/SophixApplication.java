package com.influency.itlab.base;

import android.app.Application;
import android.util.Log;

import io.reactivex.rxjava3.plugins.RxJavaPlugins;

public class SophixApplication extends Application {
    String TAG = SophixApplication.class.getSimpleName();
    @Override
    public void onCreate() {
        super.onCreate();
        RxJavaPlugins.setErrorHandler(throwable -> Log.e(TAG,throwable.toString()));
    }
}

package com.influency.itlab.activity;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.influency.itlab.R;
import com.influency.itlab.http.base.Api;
import com.influency.itlab.http.base.NetEngine;
import com.influency.itlab.http.base.RetrofitManager;
import com.influency.itlab.http.model.Coupon;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

public class ScrollingActivity extends AppCompatActivity {
    static final String TAG = ScrollingActivity.class.getSimpleName();

    @Override
    protected void onResume() {
        super.onResume();
    }

    private Disposable disposable;

    private void fetchNetData() {
//        39.909187,116.397451
//        disposable = RetrofitManager.getInstance().create().getActivateInfo(39.909187+"",116.397451+"",21)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(activateInfoModel -> {
//                    if(activateInfoModel == null){
//                        Log.e(TAG,"activateInfoModel null");
//                        return;
//                    }
//                    Toast.makeText(ScrollingActivity.this,activateInfoModel+"",Toast.LENGTH_LONG).show();
//                });
//        disposable = RetrofitManager.getInstance().create(Api.class).listCoupons()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe((coupons -> {
//                    Toast.makeText(ScrollingActivity.this, coupons + "", Toast.LENGTH_LONG).show();
//                }));
        disposable = NetEngine.getInfo(
                39.909187 + "",
                116.397451 + "", (activateInfo -> Toast.makeText(ScrollingActivity.this, activateInfo + "", Toast.LENGTH_LONG).show()));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fetchNetData();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
